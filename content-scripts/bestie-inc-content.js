var links = [];
var validDataImages = [].slice.apply(document.getElementsByTagName("img"));
validDataImages = validDataImages.filter(function (element) {
    let flag = false;
    if (element.getAttribute("data-srcset")) {
        flag = true;
    }
    return flag;
}).map(function (element) {
    var dataImage = {};
    var imageAlt = element.getAttribute("alt");
    imageAlt = imageAlt.replace(/&amp;/g, "&").replace(/&gt;/g, ">").replace(/&lt;/g, "<")
        .replace(/&quot;/g, "\"").replace(/&#39;/g, "\'").replace(/[\/:*?\"<>|]/g, "");
    dataImage["name"] = imageAlt + ".jpeg";
    var imageSourceSets = element.getAttribute("data-srcset");
    if (imageSourceSets) {
        var listImageSourceSet = imageSourceSets.split(",");
        listImageSourceSet.sort();
        for (const imageSourceSet of listImageSourceSet) {
            var dataImageSource = imageSourceSet.trim().split(" ");
            var imageWidth = dataImageSource[dataImageSource.length - 1];
            imageWidth = imageWidth.replace("w", "").trim();
            if (Number(imageWidth) >= 1000) {
                dataImage["src"] = dataImageSource[0].trim();
                return dataImage;
            }
        }
    }
});
validDataImages = validDataImages.filter(function (item) {
    if (item) {
        return true;
    }
    return false;
})
if (validDataImages.length <= 12) {
    links = validDataImages;
}
else {
    links = validDataImages.slice(validDataImages.length - 12, validDataImages.length);
}
chrome.runtime.sendMessage(links);