async function etsyCrawler() {
    var links = []
    var validProductLinks = [].slice.apply(document.getElementsByTagName("a"));
    validProductLinks = validProductLinks.filter(function (element) {
        const validAttribute = "listing-link";
        let flag = false;
        if (element.getAttribute("class") && element.getAttribute("href") && element.getAttribute("class").includes(validAttribute)) {
            flag = true;
        }
        return flag;
    }).map(function (element) {
        return element.getAttribute("href");
    });
    if (validProductLinks.length > 64) {
        validProductLinks = validProductLinks.slice(0, 64);
    }
    for (const validProductLink of validProductLinks) {
        var maxCall = 3;
        let doc = null;
        while (maxCall > 0) {
            try {
                var response = await fetch(validProductLink);
                var html = await response.text();
                let parser = new DOMParser();
                doc = parser.parseFromString(html, "text/html");
                break;
            } catch {
                maxCall--;
            }
        }
        if (doc) {
            var scriptTags = [].slice.apply(doc.head.querySelectorAll('script[type="application/ld+json"]'));
            scriptTags = scriptTags.map(function (element) {
                let dataScript = JSON.parse(element.text);
                const validType = "Product";
                if (dataScript.image && dataScript["@type"] == validType) {
                    let imageAlt = dataScript.name;
                    imageAlt = imageAlt.replace(/&amp;/g, "&").replace(/&gt;/g, ">").replace(/&lt;/g, "<")
                        .replace(/&quot;/g, "\"").replace(/&#39;/g, "\'").replace(/[\/:*?\"<>|]/g, "");
                    return { "src": dataScript.image, "name": imageAlt + ".jpeg" };
                }
            });
            if (scriptTags) {
                links.push(scriptTags[0]);
            }
        }
    }
    chrome.runtime.sendMessage(links);
}
etsyCrawler();
