var links = [];
var validImages = [].slice.apply(document.getElementsByTagName("img"));
validImages = validImages.filter(function (element) {
    let flag = false;
    let indexImage = element.getAttribute("data-image-index");
    if (indexImage && element.getAttribute("srcset") && indexImage > 0) {
        flag = true;
    }
    return flag;
}).map(function (element) {
    let listImageSrcSet = element.getAttribute("srcset").split(",");
    let imageSrcSet = listImageSrcSet[listImageSrcSet.length - 1];
    imageSrcSet = imageSrcSet.trim();
    let imageSrc = imageSrcSet.split(" ")[0];
    let imageSrcParts = imageSrc.split(".");
    imageSrcParts = imageSrcParts.filter(function (element){
        if (element.startsWith("_AC")){
            return false;
        }
        return true;
    })
    imageSrc = imageSrcParts.join(".");
    var imageAlt = element.getAttribute("alt")
    imageAlt = imageAlt.replace(/&amp;/g, "&").replace(/&gt;/g, ">").replace(/&lt;/g, "<")
        .replace(/&quot;/g, "\"").replace(/&#39;/g, "\'").replace(/[\/:*?\"<>|]/g, "");
    return { "name": imageAlt + ".jpeg", "src": imageSrc };
});
links = validImages;
chrome.runtime.sendMessage(links);